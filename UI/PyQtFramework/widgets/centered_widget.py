from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QHBoxLayout, QLabel, QWidget, QVBoxLayout


def get_centered_hbox_with_label(widget: QWidget, label: str) -> QHBoxLayout:
    hbox = QHBoxLayout()
    hbox.addWidget(QLabel(label))
    hbox.addWidget(widget)
    hbox.setAlignment(Qt.AlignHCenter)
    return hbox


def get_centered_hbox(widgets: list) -> QHBoxLayout:
    hbox = QHBoxLayout()
    for widget in widgets:
        if issubclass(type(widget), QWidget):
            hbox.addWidget(widget)
        else:
            hbox.addLayout(widget)
        hbox.setAlignment(Qt.AlignHCenter)
    return hbox


def get_centered_vbox(widgets: list) -> QVBoxLayout:
    vbox = QVBoxLayout()
    for widget in widgets:
        if issubclass(type(widget), QWidget):
            vbox.addWidget(widget)
        else:
            vbox.addLayout(widget)
        vbox.setAlignment(Qt.AlignCenter)
    return vbox
