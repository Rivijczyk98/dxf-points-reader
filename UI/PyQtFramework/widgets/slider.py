from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QLabel, QSlider, QVBoxLayout


class SliderWidget(QWidget):
    signal_slider_value_changed = pyqtSignal(int, name="ssd")

    def __init__(self, min_val, max_val, orientation=Qt.Horizontal, tick_interval=None, single_step=None,
                 is_float=False, is_uneven=False):
        super().__init__()
        self.float_precision = 100
        self.is_uneven = is_uneven
        self.is_float = is_float
        self.val = 0 if is_float else min_val
        self.slider_value = QLabel("0") if is_float else QLabel(str(min_val))
        self.min_val, self.max_val = min_val, max_val
        self.slider = self.get_slider(orientation, tick_interval, single_step)
        self.slider.valueChanged.connect(self.emit_signal_value_changed)
        self.set_layout()

    def emit_signal_value_changed(self):
        if self.is_float:
            self.val = self.slider.value() / self.float_precision
        else:
            self.val = self.slider.value()
            if self.is_uneven:
                if self.val % 2 == 0:
                    self.val -= 1
        self.slider_value.setText(str(self.val))
        self.signal_slider_value_changed.emit(self.val)

    def set_layout(self):
        vbox = QVBoxLayout()
        slider_labels = self.get_slider_labels_layout()
        vbox.addLayout(slider_labels)
        vbox.addWidget(self.slider)
        self.setLayout(vbox)

    def get_slider_labels_layout(self):
        slider_labels = QHBoxLayout()
        if self.is_float:
            slider_labels.addWidget(QLabel(str(int(self.min_val / self.float_precision))))
        else:
            slider_labels.addWidget(QLabel(str(self.min_val)))
        slider_labels.addStretch()
        slider_labels.addWidget(self.slider_value)
        slider_labels.addStretch()
        if self.is_float:
            slider_labels.addWidget(QLabel(str(int(self.max_val / self.float_precision))))
        else:
            slider_labels.addWidget(QLabel(str(self.max_val)))
        slider_labels.setContentsMargins(5, 0, 5, 0)
        return slider_labels

    def get_slider(self, orientation, tick_interval, single_step):
        slider = QSlider(orientation)
        slider.setFocusPolicy(Qt.StrongFocus)
        slider.setTickPosition(QSlider.TicksBelow)
        if tick_interval is not None:
            slider.setTickInterval(tick_interval)
        if single_step is not None:
            slider.setSingleStep(single_step)
        slider.setRange(self.min_val, self.max_val)
        return slider
