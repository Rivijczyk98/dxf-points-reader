import numpy as np
import qimage2ndarray
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QPalette, QImageReader, QImage, QPixmap
from PyQt5.QtWidgets import QWidget, QLabel, QScrollArea, QPushButton, QCheckBox, QHBoxLayout, QSizePolicy, QGridLayout, \
    QMessageBox
from qimage2ndarray import array2qimage


class ImageViewerWidget(QWidget):
    def __init__(self):
        super().__init__()
        self.img_label = QLabel()
        self.scroll_area = QScrollArea()
        self.zoom_in_btn, self.zoom_out_btn = QPushButton("+"), QPushButton("-")
        self.fit_to_window_chb = QCheckBox("Fit to window")
        self.zoom_hbox = QHBoxLayout()
        self.scale_factor = 0.1
        self.zoom_scale = 1.0
        self.zoom_min, self.zoom_max = 0.1, 6.0
        self.setup_elements()
        self.set_layout()

    def setup_elements(self):
        self.img_label.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        self.img_label.setScaledContents(True)
        self.img_label.setBackgroundRole(QPalette.Base)
        self.scroll_area.setBackgroundRole(QPalette.Base)
        self.scroll_area.setWidget(self.img_label)
        self.scroll_area.setAlignment(Qt.AlignCenter)

        self.fit_to_window_chb.stateChanged.connect(self.fit_to_window)

        self.zoom_in_btn.setFixedSize(QSize(50, 20))
        self.zoom_in_btn.clicked.connect(self.zoom_in)

        self.zoom_out_btn.setFixedSize(QSize(50, 20))
        self.zoom_out_btn.clicked.connect(self.zoom_out)

        self.zoom_hbox.addWidget(self.fit_to_window_chb)
        self.zoom_hbox.addStretch(1)
        self.zoom_hbox.addWidget(self.zoom_out_btn)
        self.zoom_hbox.addWidget(QLabel("ZOOM"))
        self.zoom_hbox.addWidget(self.zoom_in_btn)

    def set_layout(self):
        grid = QGridLayout()

        row, column = 0, 0
        grid.addLayout(self.zoom_hbox, row, column, Qt.AlignCenter)

        row, column = 1, 0
        grid.addWidget(self.scroll_area, row, column)

        self.setLayout(grid)

    def get_image(self, in_gray = False):
        pixmap = QPixmap.toImage(self.img_label.pixmap())
        if not in_gray:
            return qimage2ndarray.rgb_view(pixmap)
        else:
            return qimage2ndarray.raw_view(pixmap)

    def load_image_from_filename(self, filename: str):
        reader = QImageReader(filename)
        reader.setAutoTransform(True)
        img = reader.read()
        if img.isNull():
            QMessageBox.information(self, "Error", "Cannot load file {}. {}".format(filename, reader.errorString()))
        self.set_image(img)

    def load_image_from_array(self, img: np.ndarray):
        image = array2qimage(img)
        self.set_image(image)

    def set_image(self, img: QImage):
        self.zoom_scale = 1.0
        self.img_label.setPixmap(QPixmap.fromImage(img))
        self.img_label.adjustSize()

        frame, img_ = self.scroll_area.size(), self.img_label.size()
        if img_.width() > frame.width() or img_.height() > frame.height():
            self.scale_down_img_to_fit_the_window(frame, img_)

    def scale_down_img_to_fit_the_window(self, frame, img):
        if img.width() > frame.width() and img.width() - frame.width() > img.height() - frame.height():
            self.zoom_scale = round(frame.width() / img.width(), 2) - 0.01
        else:
            self.zoom_scale = round(frame.height() / img.height(), 2) - 0.01
        self.scaleImage()

    def fit_to_window(self):
        if self.img_label.pixmap() is not None:
            check_btn = self.fit_to_window_chb.isChecked()
            self.scroll_area.setWidgetResizable(check_btn)
            self.zoom_in_btn.setEnabled(not check_btn)
            self.zoom_out_btn.setEnabled(not check_btn)
            if not check_btn:
                self.scaleImage()
        else:
            print("No image has been loaded.")

    def scaleImage(self):
        self.img_label.resize(self.zoom_scale * self.img_label.pixmap().size())

    def zoom_in(self):
        if self.img_label.pixmap() is not None:
            if self.zoom_scale <= self.zoom_max - self.scale_factor:
                self.zoom_scale = self.zoom_scale + self.scale_factor
                self.scaleImage()
            else:
                print("Max zoom scale reached")
        else:
            print("No image has been loaded.")

    def zoom_out(self):
        if self.img_label.pixmap() is not None:
            if self.zoom_scale >= self.zoom_min + self.scale_factor:
                self.zoom_scale = self.zoom_scale - self.scale_factor
                self.scaleImage()
            else:
                print("Min zoom scale reached")
        else:
            print("No image has been loaded.")
