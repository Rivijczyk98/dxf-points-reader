def process_points_from_dxf(lines: list):
    i = -1
    coord = ['z', 'y', 'x']
    obj = []
    temp = {'x': 0.0, 'y': 0.0, 'z': 0.0, 'z_relative': 0.0}

    first_height = 999.9

    for idx, l in enumerate(lines):
        if i >= 0:
            if i % 2 == 1:
                el = int(i / 2)
                res = float(l)
                if el == 0 and first_height > 999.0:
                    first_height = float(l)
                if el == 0:
                    res = res - first_height
                    temp['z_relative'] = l.rstrip()
                temp[coord[el]] = round(res, 5)
            i -= 1
            if i < 0:
                obj.append(temp)
        elif 'AcDbPoint' in l:
            i = 6
            temp = {'x': 0.0, 'y': 0.0, 'z': 0.0, 'z_relative': 0.0}

    return obj
