from PyQt5.QtWidgets import QMessageBox, QInputDialog, QWidget


def show_info_dialog(msg_info, title="Message Box"):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setText(msg_info)
    msg.setWindowTitle(title)
    msg.setStandardButtons(QMessageBox.Ok)
    msg.exec_()


def get_text_input_dialog(parent: QWidget, msg="Input your value."):
    text, ok = QInputDialog.getText(parent, 'Input', msg)
    if ok:
        return str(text)
