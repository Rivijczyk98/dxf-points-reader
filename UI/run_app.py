from UI.PyQtFramework.config import default_config
from UI.App import LeicaReaderApp

if __name__ == "__main__":
    default_config["window_title"] = "Leica reader v.0.0.01"
    app = LeicaReaderApp(default_config)
