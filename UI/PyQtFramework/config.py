default_config = {
    "window_size_x": 1280,
    "window_size_y": 720,

    "window_position_x": -1,
    "window_position_y": -1,

    "window_title": "Program",
    "window_icon": "",

    "dark_theme": True
}
