from UI.CentralWidget import CentralWidget
from UI.PyQtFramework.app import App


class LeicaReaderApp(App):
    def init_central_widget(self):
        self.central_widget = CentralWidget()
        self.setCentralWidget(self.central_widget)

    def init_menu(self):
        pass

    def __init__(self, configuration):
        super().__init__(configuration)
