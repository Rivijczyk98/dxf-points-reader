from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel

from UI.PyQtFramework.widgets.lines import QHLine


class OutlinedHeaderWidget(QWidget):
    def __init__(self, title):
        super().__init__()
        vbox = QVBoxLayout()
        vbox.addWidget(QHLine())
        vbox.addWidget(QLabel(title), alignment=Qt.AlignCenter)
        vbox.addWidget(QHLine())
        self.setLayout(vbox)