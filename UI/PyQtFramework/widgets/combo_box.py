from PyQt5.QtWidgets import QComboBox


def get_select_box_from_enum(enum_class):
    select = QComboBox()
    select.addItems(enum_class.values())
    return select
