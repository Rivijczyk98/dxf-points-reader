from PyQt5.QtWidgets import QWidget, QVBoxLayout
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar


class MplCanvasWidget(QWidget):
    def __init__(self, width=5, height=4, dpi=100):
        super().__init__()
        self.canvas = MplCanvas(width, height, dpi)
        vbox = QVBoxLayout()
        toolbar = NavigationToolbar(self.canvas, self)
        vbox.addWidget(self.canvas)
        vbox.addWidget(toolbar)
        self.setLayout(vbox)


class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, width, height, dpi):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = self.fig.add_subplot(111)
        self.axes.xaxis.labelpad = 10
        self.axes.yaxis.labelpad = 12
        super(MplCanvas, self).__init__(self.fig)

    def set_title(self, title, pad=10):
        self.axes.set_title(title, pad=pad)

    def set_x_axis_title(self, title):
        self.axes.set_xlabel(title)

    def set_y_axis_title(self, title):
        self.axes.set_ylabel(title)

    def set_axis_ranges(self, x_min, x_max, y_min, y_max):
        self.axes.axis([x_min, x_max, y_min, y_max])

    def set_grid(self, bool_: bool):
        self.axes.grid(bool_)

    def plot_bars(self, x, y, facecolor="gray", alpha=0.95):
        self.axes.bar(x, y, alpha=alpha, facecolor=facecolor)

    def plot(self, x, y, label=None):
        if label is not None:
            self.axes.plot(x, y, label=label)
        else:
            self.axes.plot(x, y)

    def clear(self):
        self.axes.cla()

    def redraw(self):
        self.draw()

    def replot_bars(self, x, y, facecolor="gray"):
        self.plot_bars(x, y, facecolor=facecolor)

    def plot_threshold(self, t, label_, c_="r"):
        self.axes.axvline(t, label=label_, c=c_)

    def show_legend(self):
        self.axes.legend()

    def rotate_x_labels(self, labels):
        self.fig.autofmt_xdate()

    def hide_every_nth_xlabel(self, n=5):
        labels = self.axes.xaxis.get_ticklabels()
        for i in range(0, len(labels)):
            labels[i].set_visible(True if i % n == 0 else False)
