from PyQt5.QtWidgets import QWidget, QTableWidget, QPushButton, QFileDialog, QTableWidgetItem

from Algorithms.DxfProcessor import process_points_from_dxf
from UI.Dxf3dViewer import Dxf3dViewer
from UI.PyQtFramework.widgets.centered_widget import get_centered_hbox, get_centered_vbox

from UI.PyQtFramework.widgets.title_headers import OutlinedHeaderWidget


class CentralWidget(QWidget):
    def __init__(self):
        super().__init__()
        self.data_table = QTableWidget(0, 5)
        self.labels = ['number', 'x', 'y', 'z', 'z_relative']
        self.data_table.setHorizontalHeaderLabels(self.labels)
        self.data_table.setFixedHeight(250)

        self.chart_viewer = Dxf3dViewer()

        self.set_layout()

    def set_layout(self):
        select_file_button = QPushButton("Select File")
        select_file_button.setFixedWidth(200)
        select_file_button.clicked.connect(self.select_chart)

        options = get_centered_hbox([select_file_button])
        self.setLayout(get_centered_vbox(
            [options, OutlinedHeaderWidget("Points"), self.data_table, OutlinedHeaderWidget("Map"), self.chart_viewer]))

    def select_chart(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setNameFilters(["DXF files (*.dxf)"])

        if dlg.exec_():
            filenames = dlg.selectedFiles()

            f = open(filenames[0], 'r')

            with f:
                lines = f.readlines()
                if len(lines) > 0:
                    data = process_points_from_dxf(lines)
                    self.chart_viewer.plot(data)

                    self.data_table.clear()
                    self.data_table.setHorizontalHeaderLabels(self.labels)

                    self.data_table.setRowCount(len(data))
                    for i in range(len(data)):
                        for j in range(len(self.labels)):
                            if j == 0:
                                self.data_table.setItem(i, j, QTableWidgetItem(f'A{i}'))
                            else:
                                el = str(data[i][self.labels[j]])
                                self.data_table.setItem(i, j, QTableWidgetItem(el))
