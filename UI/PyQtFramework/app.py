import sys
from abc import abstractmethod
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QDesktopWidget, QMainWindow, QAction, QMenu, QFileDialog

from UI.PyQtFramework.theme import get_dark_palette


class App(QMainWindow):
    def __init__(self, configuration):
        self.app = QApplication(sys.argv)
        super().__init__()
        self.config = configuration
        self.basic_config_setup()
        self.init_central_widget()
        self.init_menu()
        self.run()

    @abstractmethod
    def init_central_widget(self):
        pass

    @abstractmethod
    def init_menu(self):
        pass

    def run(self):
        self.show()
        sys.exit(self.app.exec_())

    def basic_config_setup(self):
        if self.config["dark_theme"]:
            self.set_style()
        self.resize(self.config["window_size_x"], self.config["window_size_y"])
        if self.config["window_position_x"] == -1 or self.config["window_position_y"] == -1:
            self.center_window()
        else:
            self.move(self.config["window_position_x"], self.config["window_position_y"])
        self.setWindowTitle(self.config["window_title"])
        self.setWindowIcon(QIcon(self.config["window_icon"]))

    def center_window(self):
        frame = self.frameGeometry()
        center = QDesktopWidget().availableGeometry().center()
        frame.moveCenter(center)
        self.move(frame.topLeft())

    def set_style(self):
        self.app.setStyle('Fusion')
        self.app.setPalette(get_dark_palette())

    # WINDOW UI BUILDER METHODS --------------------------------------------------------------------

    # FILEDIALOG
    def add_openfilename_dialog(self, filetypes="All Files (*)", directory=None):
        #  example_filetypes = "All Files (*);;Python Files (*.py)"
        if directory is not None:
            filename, _ = QFileDialog.getOpenFileName(self, "", directory, filetypes)
        else:
            filename, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "", filetypes)
        return filename

    def add_savefilename_dialog(self, filetypes="Images (*.png)", directory=None):
        if directory is not None:
            filename, _ = QFileDialog.getSaveFileName(self, "", directory, filetypes)
        else:
            filename, _ = QFileDialog.getSaveFileName(self, "QFileDialog.getSaveFileName()", "", filetypes)
        return filename

    # STATUS BAR
    def show_status_bar_message(self, message="Status bar message", msecs=50000):
        self.statusBar().showMessage(message, msecs)

    def clear_status_bar_message(self):
        self.statusBar().clearMessage()

    # MENU, SUBMENU, TOOLBAR
    def add_menu(self, menu_name):
        menu = self.menuBar().addMenu(menu_name)
        return menu

    def add_submenu(self, menu, submenu_name):
        submenu = QMenu(submenu_name, self)
        menu.addMenu(submenu)
        return submenu

    def add_toolbar(self, toolbar_name):
        toolbar = self.addToolBar(toolbar_name)
        return toolbar

    def add_action(self, menu, action_name, action_callback):
        # works with menu, submenu and toolbar
        action = QAction(action_name, self)
        action.triggered.connect(action_callback)
        menu.addAction(action)

    def add_menu_check_option(self, menu, action_name: str, set_checked: bool, action_callback, status_tip=None):
        action = QAction(action_name, self, checkable=True)
        if status_tip is not None:
            action.setStatusTip(status_tip)
        action.setChecked(set_checked)
        action.triggered.connect(action_callback)
        menu.addAction(action)
