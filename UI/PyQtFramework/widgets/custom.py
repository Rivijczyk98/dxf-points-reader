from PyQt5.QtWidgets import QWidget, QVBoxLayout


class CustomWidget(QWidget):
    def __init__(self, layout=None):
        super().__init__()
        if layout is not None:
            self.setLayout(layout)
        else:
            self.setLayout(QVBoxLayout())
