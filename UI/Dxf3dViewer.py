from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure


class Dxf3dViewer(FigureCanvas):
    def __init__(self):
        self.figure = Figure()
        self.ax = None
        super().__init__(self.figure)

    def plot(self, data):
        self.figure.clear()
        self.ax = self.figure.add_subplot(projection='3d')

        x_arr, y_arr, z_arr = [], [], []

        for idx, el in enumerate(data):
            x_arr.append(el['x'])
            y_arr.append(el['y'])
            z_arr.append(el['z'])

            self.ax.text(el['x'], el['y'], el['z'], f'A{idx}')

        self.ax.scatter(x_arr, y_arr, z_arr, s=10, c='green')

        self.ax.set_xlabel('X')
        self.ax.set_ylabel('Y')
        self.ax.set_zlabel('Z')

        self.draw()
