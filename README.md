# Dxf Points Reader

Simple python program to read and show points position from dxf file created by device Leica Disco s910 function Dxf P2P.

## Requirements
* Python 3.9 or above 
* PyQt5
* MatPlotLib

## Authors and acknowledgment
Author: 
* @Rivjczyk98

Author acknowledges contribution of @saw.magdalena as creator of GUI Framework.

## Project status
Project is finished as Author doesn't need more functionalities in this program.
